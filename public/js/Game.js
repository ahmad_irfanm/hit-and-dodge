class Game {
    constructor () {
        this.play = false;
        this.players = [];
        this.ball = false;
        this.ballStartDegrees = 180 + 45;
        this.bound = {
            x: canvas.width / 2,
            y: canvas.height / 2,
            r: 120
        };
        this.maxPlayers = 2;
        this.speed = 1.5; 
        this.dirToRight = true;
        this.name = null;
        // this.name = "irfan";
    }

    setPlay () {
        this.play = true;
    }

    run () { 
        this.generate();
        this.listener();
        this.draw();
        this.update();
    }

    generate ()  {
        this.createBound();
        this.createBall();
        
        // store available players
        socket.on('store-players', (players) => { 
            // this.name = window.prompt('What is your name ?');
            this.name = "Irfan";
            this.players = [];
            players.forEach(player => this.createPlayer(player.name == this.name));
        });   

        // store play
        socket.on('run', (data) => { 
            this.setPlay();
        });

        // change direction
        socket.on('change-direction', (dir) => {
            this.changeDirection();
        });
    }

    getMyself () {
        return this.players.filter(player => player.mySelf)[0];
    }

    listener () {
        window.addEventListener('keydown', (e) => {
            let key = e.keyCode;

            if (key == 32) {
                socket.emit('play', 'data'); 
            }
        });

        document.getElementById('hit').addEventListener('touchstart', () => {
            if (this.getMyself().canHit) { 
                socket.emit('update-direction', this.dirToRight);
            }
        });

        document.getElementById('dodge').addEventListener('touchstart', () => {
            this.getMyself().r -= 60;
            setTimeout(() => {
                this.getMyself().r = this.bound.r;
            }, 1000);
        });
    }

    draw () {
        requestAnimationFrame(this.draw.bind(this));

        ctx.clearRect(0,0,canvas.width,canvas.height);

        this.bound.draw();
        this.players.forEach(player => {
            player.draw();
        });
        if (this.ball) this.ball.draw();
    }

    update () {
        requestAnimationFrame(this.update.bind(this));

        if (this.play) {
            if (this.ball) this.ball.update();
            this.players.forEach(player => player.update());
        }
    }

    createBound () {
        this.bound.draw = () => {
            ctx.beginPath();
            ctx.arc(this.bound.x, this.bound.y, this.bound.r, 0, Math.PI * 2, true);
            ctx.fillStyle = 'orange';
            ctx.fill();
            ctx.closePath();
        }
    }

    createPlayer (mySelf = false) {
        let degrees = (this.players.length * (this.maxPlayers * 90)) + 135;
        let newPlayer = new Player(degrees, mySelf);
        this.players.push(newPlayer);  
    }

    createBall () {
        this.ball = new Ball(this.ballStartDegrees);
    }

    toRadian (degree) {
        return degree * (Math.PI / 180);
    }

    collide (obj1, obj2) {
        if (obj1.x > obj2.x && obj1.x < obj2.x + obj2.w && obj1.y > obj2.y && obj1.y < obj2.y + obj2.h) {
            return true;
        }
        return false;
    }

    changeDirection () {
        this.dirToRight = !this.dirToRight;
        this.speed = this.dirToRight ? 1.5 : -1.5;
    }
}