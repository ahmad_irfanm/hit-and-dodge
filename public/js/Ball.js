class Ball {
    constructor (degree) {
        this.degree = degree;
        this.x = 0;
        this.y = 0;
        this.r = 10;
        this.radian = 0;
        this.margin = 20;

        this.update();
    }

    draw () {
        ctx.beginPath();
        ctx.fillStyle = 'red';
        ctx.arc(this.x, this.y, this.r, 0, Math.PI * 2, true);
        ctx.fill();
        ctx.closePath();
    }

    update () {
        this.radian = game.toRadian(this.degree);
        this.x = game.bound.x + Math.sin(this.radian) * (game.bound.r + this.margin);
        this.y = game.bound.y + Math.cos(this.radian) * (game.bound.r + this.margin);

        this.degree -= game.speed;
    }
}