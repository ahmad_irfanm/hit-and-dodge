class Player {
    constructor (degree, mySelf = false) {
        this.degree = degree;
        this.x = 0;
        this.y = 0;
        this.w = 40;
        this.h = 40;
        this.legalHit = 20;
        this.radian = game.toRadian(this.degree);
        this.canHit = false;
        this.ID = this.getId();
        this.hearts = 3;
        this.cooldown = false;
        this.mySelf = mySelf;
        this.r = game.bound.r;
    }

    getId () {
        return `player-${game.players.length + 1}`;
    }

    draw () {
        this.x = game.bound.x + Math.sin(this.radian) * this.r;
        this.y = game.bound.y + Math.cos(this.radian) * this.r;

        this.x -= this.w/2;
        this.y -= this.h/2;

        ctx.beginPath();
        if (this.mySelf) { 
            ctx.fillStyle = 'cyan';
        } else {
            ctx.fillStyle = 'mediumpurple';
        }   
        ctx.rect(this.x, this.y, this.w, this.h);
        ctx.fill();
        ctx.closePath();

        ctx.beginPath();
        if (this.mySelf) { 
            ctx.strokeStyle = 'cyan';
        } else {
            ctx.strokeStyle = 'mediumpurple';
        }
        ctx.rect(this.x - this.legalHit, this.y - this.legalHit, this.w + this.legalHit * 2, this.h + this.legalHit * 2);
        ctx.stroke();
        ctx.closePath();

        // draw hearts
        ctx.save();
        ctx.fillStyle = '#fff';
        ctx.font = "20px segoe ui";
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.fillText(this.hearts, this.x + this.w/2, this.y + this.h/2);
        ctx.restore();
    }

    update () {
        // game over
        if (game.collide(game.ball, this))   {
            if (!this.cooldown) {
                this.cooldown = true;
                this.hearts -= 1;  
                if (this.hearts == 0) {
                    this.gameOver();
                }
            } 
        } else {
            this.cooldown = false;
        }

        // hit
        let legalObj = {
            x: this.x - this.legalHit,
            y: this.y - this.legalHit,
            w: this.w + this.legalHit * 2,
            h: this.h + this.legalHit * 2
        };

        if (game.collide(game.ball, legalObj)) {
            this.canHit = true;
        } else {
            this.canHit = false;
        }
    }

    gameOver () {
        let removed = game.players.splice(this.ID, 1); 
        return;
    }
}