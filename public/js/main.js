window.onload = init();

function init () {
    setRealtime();

    canvas = document.getElementById('canvas');
    canvas.width = canvas.scrollWidth;
    canvas.height = canvas.scrollHeight;
    ctx = canvas.getContext('2d');

    game = new Game();
    game.run();

}

function setRealtime () {
    socket = io();
}