var express = require('express');
var app = express();
var path = require('path');
var server = require('http').Server(app);
var io = require('socket.io')(server);

app.use(express.static(path.join(__dirname + '/public')));

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

server.listen(process.env.PORT || 3000, function () {
    console.log('your node js server is running');
});

// server variables
var players = [{
    name: 'Irfan'
},{
    name: 'Aa'
}];

io.on('connection', function (socket) { 

    socket.emit('store-players', players); 

    socket.on('update-direction', function (dir) {
        socket.emit('change-direction', dir);
        socket.broadcast.emit('change-direction', dir);
    });

    socket.on('play', function(data) { 
        socket.emit('run', data);
        socket.broadcast.emit('run', data);
    });

});
